const router = require('express').Router();

const authController = require('../controllers/auth.comtroller');

router.post('/login', authController.userLogin);
router.post('/register', authController.userRegister);

module.exports = router;
