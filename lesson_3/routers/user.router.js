const router = require('express').Router();

const userController = require('../controllers/user.comtroller');

router.get('/', userController.getUsers);

router.get('/:user_email', userController.getUserByEmail);

module.exports = router;
