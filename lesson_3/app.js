const path = require('path');

const express = require('express');

const app = express();

const staticPath = path.join(__dirname, 'static');

// Base configuration
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(staticPath));

const { PORT } = require('./configs/server');
const { authRouter, userRouter } = require('./routers');

app.use('/auth', authRouter);
app.use('/users', userRouter);

app.listen(PORT, () => {
    console.log('App listen port: ', PORT);
});

app.get('/ping', (request, response) => { response.send('Ping is succefuly'); });
