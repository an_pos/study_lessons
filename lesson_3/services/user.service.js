// Base constants
const fs = require('fs');
const util = require('util');
const path = require('path');

// Promisify functions
const readFileFSPromise = util.promisify(fs.readFile);
const writeFileFSPromise = util.promisify(fs.writeFile);

const dataBasePath = path.join(__dirname, '..', 'database', 'users.json');

const getUsersData = async () => {
    const users = await readFileFSPromise(dataBasePath);

    return JSON.parse(users.toString());
};

const getUser = async (email) => {
    const users = await getUsersData();
    const currentUser = await users.find((user) => user.email === email);

    return currentUser;
};

const addUser = async (user) => {
    const users = await getUsersData();

    users.push(user);
    await writeFileFSPromise(dataBasePath, JSON.stringify(users));
};

const emailRegistered = async (email) => {
    const user = await getUser(email);

    return !!(user);
};
const userExists = async (email, password) => {
    const user = await getUser(email);

    return (user && user.email === email && user.password === password);
};

module.exports = {
    getUsersData,
    addUser,
    getUser,
    userExists,
    emailRegistered,
};
