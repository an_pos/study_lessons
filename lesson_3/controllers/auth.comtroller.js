const {
    userExists, emailRegistered, addUser
} = require('../services/user.service');

const { validateUserEmail } = require('../helpers/validator.helper');

const CODES = require('../configs/codes');

module.exports = {
    userLogin: async (request, response) => {
        const { email, password } = request.body;

        if (email && password) {
            if (validateUserEmail(email)) {
                if (await userExists(email, password)) {
                    const message = `User with email:${email} is login successfully`;

                    response.status(CODES.OK).send(message);
                } else {
                    response.status(CODES.BAD_REQUEST).send('User with this email and password not found.');
                }
            } else {
                response.status(CODES.BAD_REQUEST).send('Email address in not valid');
            }
        } else {
            response.status(CODES.BAD_REQUEST).send('One of the fields is empty');
        }
    },
    userRegister: async (request, response) => {
        const { name, email, password } = request.body;

        if (name && email && password) {
            if (validateUserEmail(email)) {
                if (await emailRegistered(email)) {
                    response.status(CODES.BAD_REQUEST).send('User with this email is already exists');
                } else {
                    await addUser({ name, email, password });

                    const message = `User with email:${email} is register successfully`;

                    response.status(CODES.OK).send(message);
                }
            } else {
                response.status(CODES.BAD_REQUEST).send('Email address in not valid');
            }
        } else {
            response.status(CODES.BAD_REQUEST).send('One of the fields is empty');
        }
    }
};
