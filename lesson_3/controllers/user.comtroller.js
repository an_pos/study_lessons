const {
    getUsersData, getUser,
} = require('../services/user.service');

const CODES = require('../configs/codes');

module.exports = {
    getUsers: async (request, response) => {
        response.json(await getUsersData());
    },
    getUserByEmail: async (request, response) => {
        const { user_email } = request.params;
        const currentUser = await getUser(user_email);

        if (currentUser) {
            response.json(currentUser);
        } else {
            response.status(CODES.NOT_FOUND).send('User not found');
        }
    },
};
