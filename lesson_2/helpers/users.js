// Base constants
const fs = require('fs');
const util = require('util');
const path = require('path');

// Promisify functions
const readFileFSPromise = util.promisify(fs.readFile);
const writeFileFSPromise = util.promisify(fs.writeFile);

const dataBasePath = path.join(__dirname, '..', 'database', 'users.json');

const getAllUsers = async () => {
    const users = await readFileFSPromise(dataBasePath);

    return JSON.parse(users.toString());
};

const addUser = async (user) => {
    const users = await getAllUsers();

    users.push(user);
    await writeFileFSPromise(dataBasePath, JSON.stringify(users));
};

const getUser = async (email) => {
    const users = await getAllUsers();
    const currentUser = await users.find((user) => user.email === email);

    return currentUser;
};

const emailRegistered = async (email) => {
    const user = await getUser(email);

    return !!(user);
};
const userExists = async (email) => {
    const user = await getUser(email);

    return (user && user.email === email);
};

const validateUserEmail = (email) => {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email.toLowerCase());
};

module.exports = {
    getAllUsers,
    addUser,
    getUser,
    userExists,
    emailRegistered,
    validateUserEmail
};
