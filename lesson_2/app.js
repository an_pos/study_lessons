// Base constants
const path = require('path');

const express = require('express');
const expressHbs = require('express-handlebars');

const app = express();

const { PORT } = require('./configs/server');

const CODES = require('./configs/codes');

const usersHelper = require('./helpers/users');

const staticPath = path.join(__dirname, 'static');

// Base configuration
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(staticPath));
app.set('view engine', '.hbs');
app.engine('.hbs', expressHbs({ defaultLayout: false }));
app.set('views', staticPath);

app.listen(PORT, () => {
    console.log('App listen port: ', PORT);
});

app.get('/', (request, response) => {
    response.redirect('/login');
});

app.get('/login', (request, response) => {
    response.render('login');
});

app.post('/login', async (request, response) => {
    const { email, password } = request.body;

    if (email && password) {
        if (await usersHelper.validateUserEmail(email)) {
            if (await usersHelper.userExists(email)) {
                response.redirect('/users');
            } else {
                response.status(CODES.BAD_REQUEST).render('login', {
                    errorMessage: 'User with this email and password not found.',
                });
            }
        } else {
            response
                .status(CODES.BAD_REQUEST)
                .render('register', { errorMessage: 'Email address in not valid' });
        }
    } else {
        response
            .status(CODES.BAD_REQUEST)
            .render('login', { errorMessage: 'One of the fields is empty' });
    }
});

app.get('/register', (request, response) => {
    response.render('register', { errorMessage: false });
});

app.post('/register', async (request, response) => {
    const { name, email, password } = request.body;

    if (name && email && password) {
        if (await usersHelper.validateUserEmail(email)) {
            if (await usersHelper.emailRegistered(email)) {
                response.status(CODES.BAD_REQUEST).render('register', {
                    errorMessage: 'User with this email is already exists.',
                });
            } else {
                await usersHelper.addUser({ name, email, password });
                response.render('back', {
                    title: 'Back to login',
                    message: 'Register is successfully',
                    link_title: 'Come back to the login page',
                    link_url: '/login',
                });
            }
        } else {
            response
                .status(CODES.BAD_REQUEST)
                .render('register', { errorMessage: 'Email address in not valid' });
        }
    } else {
        response
            .status(CODES.BAD_REQUEST)
            .render('register', { errorMessage: 'One of the fields is empty' });
    }
});

app.get('/users', async (request, response) => {
    const usersData = await usersHelper.getAllUsers();

    response.render('users', { users: usersData });
});

app.get('/users/:userEmail', async (request, response) => {
    const { userEmail } = request.params;
    const currentUser = await usersHelper.getUser(userEmail);

    if (currentUser) {
        response.render('user', currentUser);
    } else {
        response.status(CODES.NOT_FOUND).render('back', {
            title: 'User not found',
            link_title: 'Come back to the all users page',
            link_url: '/users',
        });
    }
});
