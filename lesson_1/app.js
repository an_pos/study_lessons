// Base constants
const fs = require('fs');
const util = require('util');
const path = require('path');

// Promisify functions
const readFileFSPromise = util.promisify(fs.readFile);
const readDirFSPromise = util.promisify(fs.readdir);
const renameFSPromise = util.promisify(fs.rename);
const statFSPromise = util.promisify(fs.stat);

const sortFilesByGender = (directoryName) => {
    let configData = 'none';

    if (directoryName === 'boys') configData = { correctGender: 'male', transferDir: 'girls' };
    else if (directoryName === 'girls') configData = { correctGender: 'female', transferDir: 'boys' };

    if (configData === 'none') return;

    const directoryPath = path.join(__dirname, directoryName);

    readDirFSPromise(directoryPath)
        .then((dearData) => {
            dearData.forEach((currentFileName) => {
                const currentFilePath = path.join(directoryPath, currentFileName);

                readFileFSPromise(currentFilePath)
                    .then((fileData) => {
                        const fileDataObject = JSON.parse(fileData.toString());
                        const personGender = fileDataObject.gender;

                        if (!personGender) return;

                        if (personGender !== configData.correctGender) {
                            const transferFilepath = path.join(
                                __dirname,
                                configData.transferDir,
                                currentFileName
                            );

                            renameFSPromise(currentFilePath, transferFilepath).catch(
                                (reason) => {
                                    console.log(
                                        `Error transfer file: ${currentFileName}to: ${transferFilepath}`
                                    );
                                    console.log(reason);
                                }
                            );
                        }
                    })
                    .catch((reason) => {
                        console.log('Error reading file: :', currentFileName);
                        console.log(reason);
                    });
            });
        })
        .catch((reason) => {
            console.log('Error reading directory');
            console.log(reason);
        });
};

readDirFSPromise(__dirname)
    .then((baseDirData) => {
        baseDirData.forEach((currentObjectName) => {
            statFSPromise(currentObjectName)
                .then((objectStat) => {
                    if (objectStat.isDirectory()) {
                        console.log(
                            'Sort files by gender in the directory: ',
                            currentObjectName
                        );
                        sortFilesByGender(currentObjectName);
                    }
                })
                .catch((reason) => {
                    console.log('Error get stat of the file: ', currentObjectName);
                    console.log(reason);
                });
        });
    })
    .catch((reason) => {
        console.log('Error reading base directory');
        console.log(reason);
    });
